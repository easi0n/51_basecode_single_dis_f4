/*! ----------------------------------------------------------------------------
 * @file	deca_spi.c
 * @brief	SPI access functions
 *
 * @attention
 *
 * Copyright 2013 (c) DecaWave Ltd, Dublin, Ireland.
 *
 * All rights reserved.
 *
 * @author DecaWave
 */
#include <string.h>

#include "deca_spi.h"
#include "deca_sleep.h"
#include "deca_device_api.h"
#include "port.h"

extern SPI_HandleTypeDef hspi1;


int writetospi_serial( uint16 headerLength,
			   	    const uint8 *headerBuffer,
					uint32 bodylength,
					const uint8 *bodyBuffer
				  );

int readfromspi_serial( uint16	headerLength,
			    	 const uint8 *headerBuffer,
					 uint32 readlength,
					 uint8 *readBuffer );
/*! ------------------------------------------------------------------------------------------------------------------
 * Function: openspi()
 *
 * Low level abstract function to open and initialise access to the SPI device.
 * returns 0 for success, or -1 for error
 */
int openspi(/*SPI_TypeDef* SPIx*/)
{
	// done by port.c, default SPI used is SPI1

	return 0;

} // end openspi()

/*! ------------------------------------------------------------------------------------------------------------------
 * Function: closespi()
 *
 * Low level abstract function to close the the SPI device.
 * returns 0 for success, or -1 for error
 */
int closespi(void)
{
//	while (port_SPIx_busy_sending()); //wait for tx buffer to empty

//	port_SPIx_disable();

	return 0;

} // end closespi()

/*! ------------------------------------------------------------------------------------------------------------------
 * Function: writetospi()
 *
 * Low level abstract function to write to the SPI
 * Takes two separate byte buffers for write header and write data
 * returns 0 for success, or -1 for error
 */
int writetospi_serial
(
    uint16       headerLength,
    const uint8 *headerBuffer,
    uint32       bodylength,
    const uint8 *bodyBuffer
)
{

//	int i=0;

    decaIrqStatus_t  stat ;

    stat = decamutexon() ;

		HAL_GPIO_WritePin(GPIOA, SPIx_CS, GPIO_PIN_RESET);
	  HAL_SPI_Transmit(&hspi1, (uint8_t *)headerBuffer, headerLength, HAL_MAX_DELAY);  
	  HAL_SPI_Transmit(&hspi1, (uint8_t *)bodyBuffer, bodylength, HAL_MAX_DELAY);  
    HAL_GPIO_WritePin(GPIOA, SPIx_CS, GPIO_PIN_SET);
    decamutexoff(stat) ;

    return 0;
} // end writetospi()


/*! ------------------------------------------------------------------------------------------------------------------
 * Function: readfromspi()
 *
 * Low level abstract function to read from the SPI
 * Takes two separate byte buffers for write header and read data
 * returns the offset into read buffer where first byte of read data may be found,
 * or returns -1 if there was an error
 */

int readfromspi_serial
(
    uint16       headerLength,
    const uint8 *headerBuffer,
    uint32       readlength,
    uint8       *readBuffer
)
{

//	int i=0;

     decaIrqStatus_t  stat ;

    stat = decamutexon() ;
//	  HAL_SPI_Transmit(&hspi1, (uint8_t *)headerBuffer, headerLength, 1000);  
//	  HAL_SPI_Receive(&hspi1, (uint8_t *)readBuffer, readlength, 1000);  

    /* Wait for SPIx Tx buffer empty */
    //while (port_SPIx_busy_sending());

    //SPIx_CS_GPIO->BSRR |= SPIx_CS<<16; 
	HAL_GPIO_WritePin(GPIOA, SPIx_CS, GPIO_PIN_RESET);
//	while(1);
   HAL_SPI_Transmit(&hspi1, (uint8_t *)headerBuffer, headerLength, 1000);  
 	  HAL_SPI_Receive(&hspi1, (uint8_t *)readBuffer, readlength, 1000);  


//    for(i=0; i<headerLength; i++)
//    {
//    	SPIx->DR = headerBuffer[i];

//     	//while((SPIx->SR & SPI_I2S_FLAG_RXNE) == (uint16_t)RESET);
//		   while((SPIx->SR & SPI_SR_TXE) == (uint16_t)RESET);

//     	readBuffer[0] = SPIx->DR ; // Dummy read as we write the header
//    }

//    for(i=0; i<readlength; i++)
//    {
//    	SPIx->DR = 0;  // Dummy write as we read the message body

//    	while((SPIx->SR & SPI_SR_RXNE) == (uint16_t)RESET);
// 
//	   	readBuffer[i] = SPIx->DR ;//port_SPIx_receive_data(); //this clears RXNE bit
//    }

    //SPIx_CS_GPIO->BSRR |= SPIx_CS;
    HAL_GPIO_WritePin(GPIOA, SPIx_CS, GPIO_PIN_SET);
    decamutexoff(stat) ;

    return 0;
} // end readfromspi()
